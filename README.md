# taco's super awesome frosted flake(s)

it's grrrreat! [^1]

[![nixos compatible](https://img.shields.io/static/v1?logo=nixos&logoColor=d8dee9&label=NixOS&labelColor=5e81ac&message=compatible&color=gray&style=for-the-badge)](https://nixos.org)
[![macos compatible](https://img.shields.io/static/v1?logo=Apple&logoColor=000000&label=macOS&labelColor=ffffff&message=compatible&color=gray&style=for-the-badge)](https://github.com/LnL7/nix-darwin)
[![kde compatible](https://img.shields.io/static/v1?logo=KDE&logoColor=1D99F3&label=KDE&labelColor=ffffff&message=compatible&color=gray&style=for-the-badge)](https://github.com/LnL7/nix-darwin)
[![nixos - basically black magic](https://img.shields.io/static/v1?logo=nixos&logoColor=d8dee9&label=NixOS&labelColor=5e81ac&message=basically%20black%20magic&color=d8dee9&style=for-the-badge)](https://nixos.org)
[![made with - out pants](https://img.shields.io/static/v1?label=Made%20with&message=out%20pants&color=red&style=for-the-badge)](#)


## overview

hey what's up mtv welcome to my crib

i made a nix configuration to manage my life. 
nix is pretty crazy, you should check it out!
declarative stuff is the future. 

all of these directories are self-documented (hopefully).
read the docs and you should have a good time.
if you're not having a good time,
let me know in the [issues](issues).

## table of contents
here's what this repository currently holds:

### [home](home/README.md)
home configuration, powered by home-manager. 

### [sys](sys/README.md)
my linux host configurations, powered by nixos and nix-darwin.

### [modules](modules/README.md)
bundles of packages for nixos, powered by my terrible nix scripts.

### [packages](packages/README.md)
packages for nix, powered by nixpkgs and nixos.

### [archive](archive/README.md)
the graveyard, powered by the neverending approach of time.

## license

don't worry, i got you. this'll be simple.

all of this is under the [wtfpl](LICENSE.md).
feel free to do wtf you want with them.
if you want, you can mention that i made these
and link back to this repository to help others learn nix,
but that's entirely optional!

[^1]: [cereal isn't that great for you, actually.](https://www.webmd.com/a-to-z-guides/news/20221013/7-cereals-can-no-longer-claim-healthy-label)

